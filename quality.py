#!/usr/bin/env  python2.7

from __future__ import division
from collections import defaultdict
from math import log
import random

import getopt
import sys
import os


w = {
    "jury": 1000,
    "evil": 1
}

def train(samples):
    classes, freq = defaultdict(lambda:0), defaultdict(lambda:0)
    for feats, label in samples:
        classes[label] += 1                 # count classes frequencies
        for feat in feats:
            freq[label, feat] += 1          # count features frequencies

    # for label, feat in freq:                # normalize features frequencies
    #     print "P(%s|%s) = %f" % (label, feat, freq[label, feat])

    # for c in classes:                       # normalize classes frequencies
    #     print "P(%s) = %f" % (c, classes[c])

    return classes, freq, len(samples)                    # return P(C) and P(O|C) and len of samples

def classify(classifier, feats, laplace=1):
    classes, prob, length = classifier
    return min(classes.keys(),              # calculate argmin(-log(C|O))
        key = lambda cl: -log(w[cl] * classes[cl] / length) + \
            sum(-log(( prob.get((cl, feat), 0) + laplace) / (classes[cl] + laplace * length)) for feat in feats))


def crossvalidation(alle, k):
    n = int(len(alle) / k)
    res = {}

    for laplace in (1 * 10 ** z for z in xrange(-2, 3)):
        quality = 0
        for i in xrange(n):
            tested = alle[i * k: (i + 1) * k]
            samples = alle[:(i * k)] + alle[((i + 1) * k):]
            features = [(get_features(feat), label) for label, feat in samples]
            classifier = train(features)
            for test in tested:
                if classify(classifier, get_features(test[1]), laplace) != test[0]:
                    quality += w[test[0]]
        res[laplace] = quality

    return min(res.keys(), key=lambda x: res[x])


def get_features(sample):
    features = sample.split()[1:]
    for feature in features:
        feature.strip()

    return features

def usage():
    sys.stderr.write("{0}: usage: {0} -f <file> -k <for crossvalidation>\n".format(os.path.basename(sys.argv[0])))
    sys.exit(1)

def main():

    f = None
    q = None
    past = 2000
    future = 1000

    args, rest = getopt.getopt(sys.argv[1:], "f:q:n:k:")

    for key, val in args:
        if key == "-f":
            f = val
        elif key == "-q":
            q = int(val)
        elif key == "-n":
            past = int(val)
        elif key == "-k":
            future = int(val)

    if f is None or q is None:
        usage()

    all_sessions = [line.split(";;;") \
            for line in open(f) if len(line.split(";;;")) == 3]
    first, second = 0, 0
    glob_quality = 0
    for i in xrange(min(past, future), len(all_sessions), future):
        samples = all_sessions[max(0, i - past):i]
        checks = all_sessions[i:min(len(all_sessions), i + future)]
        # print i, len(samples), len(checks)
        # laplace = crossvalidation(samples, int(past / q))
        laplace = 0.0001
        features = [(get_features(feat), label) for label, feat, some in samples]
        classifier = train(features)

        local_first, local_second = 0, 0
        quality = 0
        for check in checks:
            if len(check) > 1:
                if classify(classifier, get_features(check[1]), laplace) != check[0]:
                        quality += w[check[0]]
                        if check[0] == "jury":
                            local_first += 1
                        else:
                            local_second += 1
        # print "%d: %f" % (i, quality / float(len(checks))) 
        glob_quality += quality
        first += local_first
        second += local_second

    # sys.stdout.write("{:.2f}\n".format(glob_quality / (len(all_sessions) - min(past, future))))
    print first, first / len([session for session in all_sessions[min(past, future):] if session[0] == "jury"])
    print second, second / len([session for session in all_sessions[min(past, future):] if session[0] == "evil"])


if __name__ == "__main__":
    exit(main())