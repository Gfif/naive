#!/usr/bin/env    python

###############################---Dependences---#############################################
#                                                                                           #
#        # apt-get install build-essential python-dev libnetfilter-queue-dev python-pip     #
#        # pip install netfilterqueue scapy                                                 #
#                                                                                           #
#############################################################################################
from __future__ import division
from collections import defaultdict
from math import log as ln

from netfilterqueue import NetfilterQueue
from scapy.all import *
import getopt
import time
import os


tts = 3
w = {
    "jury": 1000,
    "evil": 1
}

def get_features(sample):
    features = sample.split()[1:]
    for feature in features:
        feature.strip()

    return features

def train(samples):
    classes, freq = defaultdict(lambda:0), defaultdict(lambda:0)
    for feats, label in samples:
        classes[label] += 1
        for feat in feats:
            freq[label, feat] += 1

    return classes, freq, len(samples)  

def classify(classifier, feats, laplace=1):
    classes, prob, length = classifier
    return min(classes.keys(),              # calculate argmin(-ln(C|O))
        key = lambda cl: -ln(w[cl] * classes[cl] / length) + \
            sum(-ln(( prob.get((cl, feat), 0) + laplace) / (classes[cl] + laplace * length)) for feat in feats))


all_sessions = [line.split(";;;") \
        for line in open("all4242.t.txt") if len(line.split(";;;")) == 3]
samples = [s[:2] for s in all_sessions]
features = [(get_features(feat), label) for label, feat in samples]

classifier = train(features)

def filter(packet):
    if TCP in packet and Raw in packet:
        load = packet[Raw].load
        print load
        return classify(classifier, get_features(load), 0.001) == "evil"


def reject(pkt):
    packet = IP(pkt.get_payload())
    if filter(packet):
        print "Attack detected"
        rule_insert = "iptables -I FORWARD 1 -p tcp -s %s --sport %s -d %s \
        	--dport %s -j REJECT --reject-with tcp-reset"
        shost = packet[IP].src
        dhost = packet[IP].dst
        sport = packet[TCP].sport
        dport = packet[TCP].dport
        os.system(rule_insert % (shost, sport, dhost, dport))
        os.system(rule_insert % (dhost, dport, shost, sport))
        pkt.accept()
        time.sleep(tts)
        rule_delete = "iptables -D FORWARD -p tcp -s %s --sport %s -d %s \
        	--dport %s -j REJECT --reject-with tcp-reset"
        os.system(rule_delete % (shost, sport, dhost, dport))
        os.system(rule_delete % (dhost, dport, shost, sport))
    else:
        pkt.accept()

def usage():
    sys.stderr.write("{0}: usage: {0} -h <host> -a <port> \
    	-n <queue-num>\n".format(os.path.basename(sys.argv[0])))
    sys.exit(1)


def main():
    
    h = None
    p = None
    n = None
    args, rest = getopt.getopt(sys.argv[1:], "h:p:n:")
    try:
        for key, val in args:
            if key == "-h":
                h = val
            elif key == "-p":
                p = int(val)
            elif key == "-n":
                n = int(val)
    except:
        usage()



    if h is None or p is None or n is None:
        usage()

    print "I'm ready"

    os.system("iptables -I FORWARD -p tcp -d %s --dport %d -j NFQUEUE --queue-num %d" \
        % (h, p, n))
    nfqueue = NetfilterQueue()
    nfqueue.bind(n, reject)

    try:
        nfqueue.run()
    except KeyboardInterrupt:
        os.system("iptables -D FORWARD -p tcp -d %s --dport %d -j NFQUEUE --queue-num %d" \
			% (h, p, n))
        print "Exiting... Please wait for %d seconds" % (tts + 3)
        time.sleep(tts + 3)

if __name__ == "__main__":
	exit(main())
