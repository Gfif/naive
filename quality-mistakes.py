#!/usr/bin/env  python2.7

from __future__ import division
from collections import defaultdict
from math import log
import random
import getopt
import sys
import os


FIRST = 0.2

past = 1000
future = 100

def train(samples):
    classes, freq = defaultdict(lambda:0), defaultdict(lambda:0)
    for feats, label in samples:
        classes[label] += 1
        for feat in feats:
            freq[label, feat] += 1

    return classes, freq, len(samples)

def classify(classifier, feats, laplace=1, w={"jury":1, "evil":1}):
    classes, prob, length = classifier
    return min(classes.keys(),
        key = lambda cl: -log(w[cl] * classes[cl] / length) + \
            sum(-log(( prob.get((cl, feat), 0) + laplace) / (classes[cl] + laplace * length)) for feat in feats))


def crossvalidation(alle, k):
    n = int(len(alle) / k)
    res = defaultdict(lambda:0)
    w0 = 1
    while w0 < 10 ** 5 or res.keys() == []:
        w = {"jury": w0, "evil": 1}
        old = 1
        for laplace in (10 ** z for z in xrange(-2, 1)):
            first, second = 0, 0
            for i in xrange(n):
                tested = alle[i * k: (i + 1) * k]
                samples = alle[:(i * k)] + alle[((i + 1) * k):]
                features = [(get_features(feat), label) for label, feat in samples]
                classifier = train(features)
                for test in tested:
                    if classify(classifier, get_features(test[1]), laplace, w) != test[0]:
                        if test[0] == "jury":
                            first += 1
                        else:
                            second += 1
            # first_mistake = first / len([session for session in alle if session[0] == "jury"])
            # print w0, laplace, first_mistake
            if first > old:
                break
            old = first
            if  first <= FIRST * len([session for session in alle if session[0] == "jury"]) or w0 > 10**10:
                res[laplace, w0] = second

        # print w0, len(res.keys())        
        w0 = w0 * 10

    return min(res.keys(), key=lambda x: res[x])


def get_features(sample):
    features = sample.split()[1:]
    for feature in features:
        feature.strip()

    return features

def usage():
    sys.stderr.write("{0}: usage: {0} -f <file> -k <for crossvalidation>\n".format(os.path.basename(sys.argv[0])))
    sys.exit(1)

def main():

    f = None
    q = None
    args, rest = getopt.getopt(sys.argv[1:], "f:q:n:k:")

    for key, val in args:
        if key == "-f":
            f = val
        elif key == "-q":
            q = int(val)
        elif key == "-n":
            past = int(val)
        elif key == "-k":
            future = int(val)

    if f is None or q is None:
        usage()

    all_sessions = [line.split(";;;") \
            for line in open(f) if len(line.split(";;;")) == 2]

    first, second = 0, 0
    for i in xrange(min(past, future), len(all_sessions), future):
        samples = all_sessions[max(0, i - past):i]
        checks = all_sessions[i:min(len(all_sessions), i + future)]
        # print i, len(samples), len(checks)
        laplace, w0 = crossvalidation(samples, q)
        w = {"jury": w0, "evil": 1}

        features = [(get_features(feat), label) for label, feat in samples]
        classifier = train(features)

        local_first, local_second = 0, 0
        for check in checks:
            if len(check) > 1:
                if classify(classifier, get_features(check[1]), laplace, w) != check[0]:
                        if check[0] == "jury":
                            local_first += 1
                        else:
                            local_second += 1

        try:
            # print "%d: %f %f" % (i, \
            #     local_first / len([session for session in checks if session[0] == "jury"]), \
            #     local_second / len([session for session in checks if session[0] == "evil"])
            # ) 
            first += local_first
            second += local_second
        except ZeroDivisionError:
            pass
    
    print first, first / len([session for session in all_sessions if session[0] == "jury"])
    print second, second / len([session for session in all_sessions if session[0] == "evil"])


if __name__ == "__main__":
    exit(main())
