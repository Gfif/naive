#!/usr/bin/env  python2.7

from __future__ import division
from collections import defaultdict
from math import log

past = 1000
future = 1000

def train(samples):
    classes, freq = defaultdict(lambda:0), defaultdict(lambda:0)
    for feats, label in samples:
        classes[label] += 1                 # count classes frequencies
        for feat in feats:
            freq[label, feat] += 1          # count features frequencies

    # for label, feat in freq:                # normalize features frequencies
    #     print "P(%s|%s) = %f" % (label, feat, freq[label, feat])

    # for c in classes:                       # normalize classes frequencies
    #     print "P(%s) = %f" % (c, classes[c])

    return classes, freq, len(samples)                    # return P(C) and P(O|C) and len of samples

def classify(classifier, feats, laplace=1):
    classes, prob, length = classifier
    for cl in classes:
        for feat in feats:
            pass
            # print "P(%s|%s)=%f" % (cl, feat, (prob.get((cl, feat), 0) + laplace) / (classes[cl] + laplace * length))
    return min(classes.keys(),              # calculate argmin(-log(C|O))
        key = lambda cl: -log(classes[cl] / length) + \
            sum(-log((prob.get((cl, feat), 0) + laplace) / (classes[cl] + laplace * length)) for feat in feats))


def crossvalidation(alle, k):
    n = int(len(alle) / k)
    values = [1 * 10 ** z for z in xrange(-2, 2)]
    res = {}

    for laplace in values:
        ef = 0
        for i in xrange(n):
            tested = alle[i * k: (i + 1) * k]
            samples = alle[:(i * k)] + alle[((i + 1) * k):]
            features = [(get_features(feat), label) for label, feat in samples]
            classifier = train(features)
            for test in tested:
                ef += classify(classifier, get_features(test[1]), laplace) == test[0]
        res[laplace] = ef

        # print "%d/%d = %f" % (ef, len(alle), float(ef) / len(alle))

    return max(res.keys(), key=lambda x: res[x])


def get_features(sample):
    features = sample.split()
    for feature in features:
        feature.strip()

    return features

def main():
    all_sessions = [line.split(";;;") for line in open('all4242.txt')]
    glob_ef = 0
    for i in xrange(min(past, future), len(all_sessions), future):
        samples = all_sessions[max(0, i - past):i]
        checks = all_sessions[i:min(len(all_sessions), i + future)]
        print i, len(samples), len(checks)
        laplace = crossvalidation(samples, 15)

        features = [(get_features(feat), label) for label, feat in samples]
        classifier = train(features)

        ef = 0
        for check in checks:
            if len(check) > 1:
                ef += classify(classifier, get_features(check[1]), laplace) == check[0]
        print "%d: %d / %d = %f" % (i, ef, len(checks), ef / len(checks)) 
        glob_ef += ef
    print "%d / %d = %f" % (glob_ef, len(all_sessions) - min(past, future), glob_ef / (len(all_sessions) - min(past, future)))


if __name__ == "__main__":
    exit(main())
