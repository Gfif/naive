#!/usr/bin/env  python2.7

import sys
import getopt
import time

from scapy.all import *

def get_load(session, address, n):
    raw_string = ""
    for packet in session:
        if Raw in packet and packet[IP].dst == address:
            raw_string += packet[Raw].load


    return " ".join(raw_string.split("\n")[:n])

def get_time(session):
    return session[0].time

def get_files_from_directory(directory):
    return [f for f in os.listdir(directory) if os.path.isfile(os.path.join(directory, f))]


def parse(directory, address, n):
    dots = []
    files_with_sessions = get_files_from_directory(directory)
    files_with_sessions.sort()
    i = 0
    for session_file in files_with_sessions:
        try:
            session = rdpcap(directory + '/' + session_file)
            if session:
                label = "evil"
                if session_file.find("jury") != -1:
                    label = "jury"
                
                load = get_load(session, address, n)
                k = 0

                print label + ";;;" + load.replace("\n", " ") + ";;;" + str(get_time(session))      

        except Scapy_Exception as msg:
            print(msg)
    
    return dots


def usage():
    sys.stderr.write("{0}: usage: {0} -d <directory_with_session_files> -a <self_ip_addr> -n <number of packets>\n".format(os.path.basename(sys.argv[0])))
    sys.exit(1)


def main():
    d = None
    a = None
    n = None
    args, rest = getopt.getopt(sys.argv[1:], "d:a:n:")

    for key, val in args:
        if key == "-d":
            d = val
        elif key == "-a":
            a = val
        elif key == "-n":
            n = int(val)

    if d is None or a is None or n is None:
        usage()

    parse(d, a, n)
    
if __name__ == "__main__":
    exit(main())
